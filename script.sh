#!/bin/bash

kubectl apply -f  services.yml
kubectl apply -f  pvc.yml
kubectl apply -f  deploy_db.yml
kubectl apply -f  deploy_app.yml
