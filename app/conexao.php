<?php
$servername = "mysql-connection";
$username = file_get_contents("/etc/secret-volume/DATABASE_USER");
$password = file_get_contents("/etc/secret-volume/ROOT_PASSWORD");
$database = file_get_contents("/etc/secret-volume/MYSQL_DATABASE");

// Criar conexão


$link = new mysqli($servername, $username, $password, $database);

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

?>
